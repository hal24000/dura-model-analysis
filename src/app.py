import pandas as pd
from cvwb.data.image_sources import get_cv_image_sources_from_json
from cvwb.data.evaluation import compute_IoU
import streamlit as st
from cvwb.data.storage import get_cv_image
from cvwb.data import cv_models
import copy
from PIL import Image

DATA_DIR = "cache_data/"

def crop_annotations(cv_image):
    single_annotation_cv_images = []

    for i, annotation_rectangle in enumerate(cv_image.annotation_rectangles()):
        new_image = cv_image.image.crop(tuple(annotation_rectangle))
        new_image_source = cv_image.image_source.copy(deep=True)
        new_image_source.annotations = [cv_image.image_source.annotations[i]]

        single_annotation_cv_images.append(cv_models.CVImage(image=new_image,
                                                             image_source=new_image_source))

    return single_annotation_cv_images


pred_image_sources = get_cv_image_sources_from_json(f"{DATA_DIR}pred_images_markings.json")
true_image_sources = get_cv_image_sources_from_json(f"{DATA_DIR}true_images_markings.json")

IoU_th = 0.25
CONFIDENCE_TH = 0.4
data = []  # frame_id, match, predicted, max_confidence, true

for img_src_idx, image_source in enumerate(true_image_sources):
    pred_image_source = pred_image_sources[img_src_idx]

    max_confidence = 0
    matched = 0
    matched_list = []

    for ann_idx, true_annotation in enumerate(image_source.annotations):

        for j, pred_annotation in enumerate(pred_image_source.annotations):
            if pred_annotation.confidence < CONFIDENCE_TH:
                continue

            max_confidence = max(max_confidence, pred_annotation.confidence)

            if compute_IoU(ref_ann=true_annotation,
                           ann=pred_annotation) >= IoU_th:
                matched += 1
                matched_list.append((ann_idx, j))
                break
    predicted = len([ann for ann in pred_image_source.annotations if ann.confidence >= CONFIDENCE_TH])
    data.append([
        image_source.frame_id,
        matched,
        predicted,
        max_confidence,
        len(image_source.annotations),
        img_src_idx,
        abs(len(image_source.annotations) - predicted),
        matched_list
    ])

original_df = pd.DataFrame(data=data,
                           columns=["frame_id", "matched_predictions", "predicted_annotations", "max_confidence",
                                    "true_annotations", "image_src_index", "pred_difference", "matched_list"])

# Sortings:
sorting_type = st.sidebar.selectbox(label="Select samples type",
                                    options=["Correctly predicted", "Biggest difference", "Potential false alarms"])
# 1. Maximal difference
if sorting_type == "Correctly predicted":
    df = original_df.sort_values(by="pred_difference", ascending=True).reset_index(drop=True)
    df = df[df.true_annotations > 0].reset_index(drop=True)
elif sorting_type == "Biggest difference":
    df = original_df.sort_values(by="pred_difference", ascending=False).reset_index(drop=True)
else:
    df = original_df[original_df.true_annotations == 0].reset_index(drop=True)
    df = df.sort_values(by="pred_difference", ascending=False).reset_index(drop=True)


#
#
# # 2. Most correct predictions


st.title("Dura Vermeer")
st.subheader("Overall validation dataset statistics")

cols = st.columns(2)
cols[0].text("Images")
cols[0].success(len(original_df))
cols[1].text("Number of annotations")
cols[1].success(original_df.true_annotations.sum())
cols = st.columns(2)
cols[0].text("Number of predicted annotations")
cols[0].warning(original_df.predicted_annotations.sum())
cols[1].text("Number of correctly matched annotations")
cols[1].warning(original_df.matched_predictions.sum())

st.subheader("Single image analysis")

image_index = st.selectbox("Choose the image",
                           options=list(range(10)))

pred_selected_image_source = pred_image_sources[df.image_src_index.values[image_index]]
true_selected_image_source = true_image_sources[df.image_src_index.values[image_index]]

image = Image.open(f"{DATA_DIR}{true_selected_image_source.frame_id}.jpg")
true_cv_image = cv_models.CVImage(image=image, image_source=true_selected_image_source)
pred_cv_image = cv_models.CVImage(image=image, image_source=pred_selected_image_source)
pred_cv_image.image_source.annotations = [ann for ann in pred_selected_image_source.annotations if
                                          ann.confidence >= CONFIDENCE_TH]

matched_annotations = df.matched_list.values[image_index]
true_indexes_matched = set([arr[0] for arr in matched_annotations])
pred_indexes_matched = set([arr[1] for arr in matched_annotations])

true_cropped = crop_annotations(true_cv_image)
pred_cropped = crop_annotations(pred_cv_image)

cols = st.columns(2)
cols[0].image(true_cv_image.annotated_image(),
              caption=f"{len(true_cv_image.image_source.annotations)} annotations found by the annotators")
cols[1].image(pred_cv_image.annotated_image(),
              caption=f"{len(pred_cv_image.image_source.annotations)} annotations found by the model")

st.header("Zoomed annotations")
show_subheader = True
expander_idx = 0

for true_index, pred_index in matched_annotations:
    if show_subheader:
        show_subheader = False
        st.subheader("Correctly predicted annotations")

    with st.expander(f"{expander_idx}", expanded=False):
        cols = st.columns(2)
        cols[0].image(true_cropped[true_index].image, caption='Annotated by annotators', use_column_width=True)
        cols[1].image(pred_cropped[pred_index].image, caption='Annotated by model', use_column_width=True)
        expander_idx += 1

expander_idx = 0
show_subheader = True
for idx, cv_annotation in enumerate(true_cropped):
    if idx in true_indexes_matched:
        continue

    if show_subheader:
        show_subheader = False
        st.subheader("Annotations not found by the model")

    with st.expander(f"{expander_idx}", expanded=False):
        expander_idx += 1
        st.image(cv_annotation.image)

show_subheader = True
expander_idx = 0
for idx, cv_annotation in enumerate(pred_cropped):
    if idx in pred_indexes_matched:
        continue

    if show_subheader:
        show_subheader = False
        st.subheader("Annotations not found by the annotators")

    with st.expander(f"{expander_idx}", expanded=False):
        expander_idx += 1
        st.image(cv_annotation.image)
